#include <iostream>
#include <iomanip>

#define C_const 299792458.
#define C_name 'c'
#define C_unit "m/s"
#define C_desc "Predkosc swiatla"

#define G_const 6.6742868e-11
#define G_name 'G'
#define G_unit "m^3/kg/s^2"
#define G_desc "Stala grawitacji"

#define R_const 8.31447215
#define R_name 'R'
#define R_unit "J*/mol/K"
#define R_desc "Stala gazowa"

#define F_const 96485.339915
#define F_name 'F'
#define F_unit "C/mol"
#define F_desc "Stala Faradaya"

#define K_const 1.380649e-23
#define K_name 'k'
#define K_unit "J/K"
#define K_desc "Stala Boltzmana"
using namespace std;

void pisz(char nam,float con, string uni, string des){
    cout <<left<<setw(5)<<nam;
    cout <<left<<setw(20)<<con;
    cout <<setw(20)<<uni;
    cout <<setw(25)<<des;
    cout << endl;
}
int main()
{
    setw(20);
    cout <<"Tabela stalych fizycznych"<<endl;
    cout << "Nazwa"<<setw(20)<< "Warosc"<<setw(20)<< "Jednostka" <<setw(25)<<"Opis"<<endl;
    pisz(C_name,C_const,C_unit,C_desc);
    pisz(G_name,G_const,G_unit,G_desc);
    pisz(R_name,R_const,R_unit,R_desc);
    pisz(F_name,F_const,F_unit,F_desc);
    pisz(K_name,K_const,K_unit,K_desc);
    return 0;
}

