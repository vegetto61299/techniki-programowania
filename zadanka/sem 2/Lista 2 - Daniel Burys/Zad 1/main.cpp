#include <iostream>
#include <windows.h>
using namespace std;
void plan(char tab[3][3]) {
    cout<<tab[0][0]<<" | "<<tab[0][1]<<" | "<<tab[0][2]<<endl;
    cout<<"----------"<<endl;
    cout<<tab[1][0]<<" | "<<tab[1][1]<<" | "<<tab[1][2]<<endl;
    cout<<"----------"<<endl;
    cout<<tab[2][0]<<" | "<<tab[2][1]<<" | "<<tab[2][2]<<endl;
}
bool sprawdz(char tab[3][3]) {
    if(tab[0][0]=='O'&&tab[0][1]=='O'&&tab[0][2]=='O'||
            tab[1][0]=='O'&&tab[1][1]=='O'&&tab[1][2]=='O'||
            tab[2][0]=='O'&&tab[2][1]=='O'&&tab[2][2]=='O'||
            tab[0][0]=='O'&&tab[1][0]=='O'&&tab[2][0]=='O'||
            tab[0][1]=='O'&&tab[1][1]=='O'&&tab[2][1]=='O'||
            tab[0][2]=='O'&&tab[1][2]=='O'&&tab[2][2]=='O'||
            tab[0][0]=='O'&&tab[1][1]=='O'&&tab[2][2]=='O'||
            tab[2][0]=='O'&&tab[1][1]=='O'&&tab[0][2]=='O') {
        cout<< "Gracz 1 wygral"<<endl;
        plan(tab);
        return 1;
    } else if (
        tab[0][0]=='X'&&tab[0][1]=='X'&&tab[0][2]=='X'||
        tab[1][0]=='X'&&tab[1][1]=='X'&&tab[1][2]=='X'||
        tab[2][0]=='X'&&tab[2][1]=='X'&&tab[2][2]=='X'||
        tab[0][0]=='X'&&tab[1][0]=='X'&&tab[2][0]=='X'||
        tab[0][1]=='X'&&tab[1][1]=='X'&&tab[2][1]=='X'||
        tab[0][2]=='X'&&tab[1][2]=='X'&&tab[2][2]=='X'||
        tab[0][0]=='X'&&tab[1][1]=='X'&&tab[2][2]=='X'||
        tab[2][0]=='X'&&tab[1][1]=='X'&&tab[0][2]=='X') {
        cout<< "Gracz 2 wygral"<<endl;
        plan(tab);
        return 1;
    } else
        return 0;
}

int main() {
    int a,b;
    char tab[3][3]= {' '};
    plan(tab);
    while(1==1) {
        while(1==1) {
            cout<<endl<<"gracz 1(O):"<<endl;
            cout<<"wiersz:";
            cin>>a;
            a--;
            cout<<"kolumna:";
            cin>>b;
            b--;
            if(a>=3||b>=3) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            if(tab[a][b]=='X'||tab[a][b]=='O') {
                cout<<"Pole zajete"<<endl;
                continue;
            }
            else {
                tab[a][b]='O';
                break;
            }
        }
        if (sprawdz(tab))
            break;
        system("cls");
        plan(tab);
        while(1==1) {
            cout<<endl<<"gracz 2(X):"<<endl;
            cout<<"wiersz:";
            cin>>a;
            a--;
            cout<<"kolumna:";
            cin>>b;
            b--;
            if(a>=3||b>=3) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            else if(tab[a][b]=='X'||tab[a][b]=='O') {
                cout<<"Pole zajete"<<endl;
                continue;
            }  else {
                tab[a][b]='X';
                break;
            }
        }
        if (sprawdz(tab))
            break;
        system("cls");
        plan(tab);
    }
    return 0;
}

