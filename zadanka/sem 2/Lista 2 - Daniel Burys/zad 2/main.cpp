#include <iostream>
#include <stdlib.h>
#include <iomanip>
using namespace std;

int main() {
    int **p;
    int a;
    int i;
    int j;
    cout << "Podaj rozmiar tablicznki mnozenia(10->0x10)" << endl;
    cin>>a;
    a++;
    p=(int**)malloc(a*sizeof(int*));

    for(i=0; i<a; i++) {
        p[i]=(int*)malloc(a*sizeof(int));
    }
    for(i=0; i<a; i++) {
        for(j=0; j<a; j++) {
            p[i][j]=i*j;
        }//cout<<endl;
    }
    cout<<"    ";
    for(i=0; i<a; i++)
        cout<<"|"<<left<<setw(4)<<i;
    cout<<endl<<setfill('-')<<setw(5*a+3)<<""<<setfill(' ');
    for(i=0; i<a; i++){
        cout<<endl<<setw(4)<<i;
        for(j=0; j<a; j++) {
            cout<<"|"<<left<<setw(4)<<i*j;
        }
    }
    return 0;
}
