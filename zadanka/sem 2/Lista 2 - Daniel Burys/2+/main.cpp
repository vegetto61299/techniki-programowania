#include <iostream>
#include <string.h>
#include <iomanip>
#include <windows.h>
#include <cstdlib>
using namespace std;
void plan(int tab[4][4][4],int n) {
    HANDLE hOut;
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );

    int i=0,j,k;
    for(i; i<n; i+=2) {
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
        cout<<setw(12)<<""<<"poziom "<<i+1;
        cout<<setw(18)<<"";
        cout<<"poziom "<<i+2;
        cout<<endl;
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
        cout<<"    - -";
        for(j=0; j<n; j++)
            cout<<setw(2)<<j+1<<" - ";
        cout<<"   - -";
        for(j=0; j<n; j++)
            cout<<setw(2)<<j+1<<" - ";
        cout<<endl<<"   | ";
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
        cout<<setfill('-')<<setw(n*5+1)<<"";
        setfill(' ');
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
        cout<<"   | ";
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
        cout<<setfill('-')<<setw(n*5+1)<<""<<setfill(' ');
        cout<<endl;
        for(j=0; j<n; j++) {
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
            cout<<"  "<<setw(2)<<j+1<<" ";
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
            cout<<"| ";
            for(k=0; k<n; k++) {
                if(tab[i][j][k]==1) {
                    SetConsoleTextAttribute( hOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                    cout<<setw(2)<<tab[i][j][k];
                    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                    cout<<" | ";
                } else if(tab[i][j][k]==2) {
                    SetConsoleTextAttribute( hOut,FOREGROUND_RED | FOREGROUND_INTENSITY);
                    cout<<setw(2)<<tab[i][j][k];
                    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                    cout<<" | ";
                } else
                    cout<<setw(2)<<tab[i][j][k]<<" | ";
            }
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
            cout<<"  "<<j+1;
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);

            cout<<" | ";
            for(k=0; k<n; k++) {
                if(tab[i+1][j][k]==1) {// +1 bo to prawa plansza
                    SetConsoleTextAttribute( hOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                    cout<<setw(2)<<tab[i+1][j][k];
                    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                    cout<<" | ";
                } else if(tab[i+1][j][k]==2) {
                    SetConsoleTextAttribute( hOut,FOREGROUND_RED | FOREGROUND_INTENSITY);
                    cout<<setw(2)<<tab[i+1][j][k];
                    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                    cout<<" | ";
                } else
                    cout<<setw(2)<<tab[i+1][j][k]<<" | ";
            }

            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
            cout<<endl<<"   | ";
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
            cout<<setfill('-')<<setw(n*5+1)<<""<<setfill(' ');
            setfill(' ');
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
            cout<<setw(2)<<""<<" | ";
            SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
            cout<<setfill('-')<<setw(n*5+1)<<""<<setfill(' ');
            cout<<endl;
        }
        cout<<endl;
    }
}
bool sprawdz(int tab[4][4][4],int n,int a,int b, int c) { // na wygran� jest 13 sposob�w/ kierunkow tyle ile polaczen miedzy dwiema scianami/wierzcholkami szescianu
    int p=tab[a][b][c];
    if( tab[a][b][0]==p&&tab[a][b][1]==p&&tab[a][b][2]==p&&tab[a][b][3]==p||//poziom
        tab[0][b][0]==p&&tab[1][b][1]==p&&tab[2][b][2]==p&&tab[3][b][3]==p||//3d skos w poziomie 1
        tab[3][b][0]==p&&tab[2][b][1]==p&&tab[1][b][2]==p&&tab[0][b][3]==p||//3d skos w poziomie 2
        tab[a][0][c]==p&&tab[a][1][c]==p&&tab[a][2][c]==p&&tab[a][3][c]==p||//pion
        tab[0][b][c]==p&&tab[1][b][c]==p&&tab[2][b][c]==p&&tab[3][b][c]==p||//pion 3d
        tab[0][0][c]==p&&tab[1][1][c]==p&&tab[2][2][c]==p&&tab[3][3][c]==p||//3d skos w pionie 1
        tab[0][3][c]==p&&tab[1][2][c]==p&&tab[2][1][c]==p&&tab[3][0][c]==p||//3d skos w pionie 2
        tab[0][0][0]==p&&tab[1][1][1]==p&&tab[2][2][2]==p&&tab[3][3][3]==p||//skos 3d 1
        tab[0][3][3]==p&&tab[1][2][2]==p&&tab[2][1][1]==p&&tab[3][0][0]==p||//skos 3d 2
        tab[0][0][3]==p&&tab[1][1][2]==p&&tab[2][2][1]==p&&tab[3][0][3]==p||//skos 3d 3
        tab[0][3][0]==p&&tab[1][2][1]==p&&tab[2][1][2]==p&&tab[3][0][3]==p||//skos 3d 4
        tab[a][0][0]==p&&tab[a][1][1]==p&&tab[a][2][2]==p&&tab[a][3][3]==p||//skos 2d
        tab[a][3][3]==p&&tab[a][2][2]==p&&tab[a][1][1]==p&&tab[a][0][0]==p)//skos 2d
        return 1;
    else
        return 0;

}
//********************************************** Teraz zabawna czesc ******************************************************
int main() {
    HANDLE hOut;
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    int n=4,a,b,c;
    system("cls");
    cout<<endl;
    int tab[4][4][4]= {0};
    plan(tab,n);
    //****************************************** LETS PLAY A GAME ******************************************************
    while(1==1) {
        while(1==1) {
            cout<<endl<<"gracz 1:"<<endl;
            a=0;
            cout<<"wiersz:";
            cin>>b;
            b--;
            cout<<"kolumna:";
            cin>>c;
            c--;
            if(b>=n||c>=n||a<0||b<0||c<0) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            while(tab[a][b][c]==1||tab[a][b][c]==2)
                a++;
            if(tab[n-1][b][c]==1||tab[n-1][b][c]==2) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                tab[a][b][c]=1;
                break;
            }
        }
        if (sprawdz(tab,n,a,b,c)) {
            system("cls");
            plan(tab,n);
            cout<<endl<<"Wygral gracz 1";
            break;
        }
        system("cls");
        plan(tab,n);
        while(1==1) {
            cout<<endl<<"gracz 2:"<<endl;
            a=0;
            cout<<"wiersz:";
            cin>>b;
            b--;
            cout<<"kolumna:";
            cin>>c;
            c--;
            if(a>=n||b>=n||c>=n||a<0||b<0||c<0) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            while(tab[a][b][c]!=0)
                a++;
            if(tab[3][b][c]==1||tab[3][b][c]==2) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                tab[a][b][c]=2;
                break;
            }
        }
        if (sprawdz(tab,n,a,b,c)) {
            system("cls");
            plan(tab,n);
            cout<<endl<<"Wygral gracz 2";
            break;
        }
        system("cls");
        plan(tab,n);
    }
    return 0;
}
