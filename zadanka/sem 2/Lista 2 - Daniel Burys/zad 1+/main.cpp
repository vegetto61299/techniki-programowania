#include <iostream>
#include <string.h>
#include <iomanip>
#include <windows.h>
#include <cstdlib>
using namespace std;
void plan(int **tab,int n) {
    HANDLE hOut;
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
    int i,j;
    cout<<"    - -";
    for(i=0; i<n; i++)
        cout<<setw(2)<<i+1<<" - ";
    cout<<endl<<"   | ";
    SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
    cout<<setfill('-')<<setw(n*5+1)<<""<<setfill(' ');
    cout<<endl;
    for(i=0; i<n; i++) {
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
        cout<<"  "<<setw(2)<<i+1<<" ";
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
        cout<<"| ";
        for(j=0; j<n; j++) {
            if(tab[i][j]==1) {
                SetConsoleTextAttribute( hOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout<<setw(2)<<tab[i][j];
                SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                cout<<" | ";
            } else if(tab[i][j]==2) {
                SetConsoleTextAttribute( hOut,FOREGROUND_RED | FOREGROUND_INTENSITY);
                cout<<setw(2)<<tab[i][j];
                SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
                cout<<" | ";
            } else
                cout<<setw(2)<<tab[i][j]<<" | ";
        }
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN );
        cout<<endl<<"   | ";
        SetConsoleTextAttribute( hOut, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
        cout<<setfill('-')<<setw(n*5+1)<<""<<setfill(' ')<<endl;
    }
}
bool sprawdz(int **tab,int n,int a,int b) {
    int i=1,p=tab[a][b];
    int c=a,d=b;
    // *************************poziom
    while(d<n&&tab[c][d+1]==p){
            i++;
        d++;
    }
    d=b;
        while(d>=1&&tab[c][d-1]==p){
            i++;
        d--;
    }

    if(i>=4)return 1;
    i=1;c=a;d=b;
    // **************************pion
    while(c<n-1&&tab[c+1][d]==p){
            i++;
        c++;
    }
    c=a;
        while(c>=1&&tab[c-1][d]==p){
            i++;
        c--;
    }
    if(i>=4)return 1;
    i=1;c=a;d=b;
    // **************************skos 1
    while(c<n-1&&d<n-1&&tab[c+1][d+1]==p){
        i++;
        c++;
        d++;
    }
    c=a;d=b;
        while(c>=1&&tab[c-1][d-1]==p){
            i++;
        c--;
        d--;
    }
    if(i>=4)return 1;
    i=1;c=a;d=b;
        // **************************skos 2
    while(c<n-1&&d>=1&&tab[c+1][d-1]==p){
        i++;
        c++;
        d--;
    }
    c=a;d=b;
        while(c>=1&&d<n-1&&tab[c-1][d+1]==p){
            i++;
        c--;
        d+1;
    }
    if(i>=4)return 1;
    i=1;c=a;d=b;
    return 0;
}
//********************************************** Teraz zabawna czesc ******************************************************
int main() {
    HANDLE hOut;
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    int n,i,j,a,b;
    cout << "Grac bedziesz na kwadratowej planszy, podaj jej rozmiar ale nie przesadz aby plansza wyswietlila sie poprawnie:" << endl;
    cin>>n;
    system("cls");
    cout<<endl;
    int **tab=new int*[n];
    for(i=0; i<n; i++) {
        tab[i]=new int[n];
        for(j=0; j<n; j++) {
            tab[i][j]=0;
        }
    }
    plan(tab,n);
    //****************************************** LETS PLAY A GAME ******************************************************
    while(1==1) {
        while(1==1) {
            cout<<endl<<"gracz 1:"<<endl;
            cout<<"kolumna:";
            cin>>b;
            b--;

            a=n-1;

            while(a>0&&tab[a][b]!=0)a--;

            if(a>=n||b>=n) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            } else if(tab[a][b]==1||tab[a][b]==2) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                tab[a][b]=1;
                break;
            }
        }
        if (sprawdz(tab,n,a,b)) {
            cout<<endl<<"Wygral gracz 1";
            break;
        }
        system("cls");
        plan(tab,n);
        while(1==1) {
            cout<<endl<<"gracz 2:"<<endl;

            cout<<"kolumna:";
            cin>>b;
            b--;
            a=n-1;
            while(a>0&&tab[a][b]!=0)a--;

            if(a>n||b>=n) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            } else if(tab[a][b]==1||tab[a][b]==2) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                tab[a][b]=2;
                break;
            }
        }
        if (sprawdz(tab,n,a,b)) {
            cout<<endl<<"Wygral gracz 2";
            break;
        }
        system("cls");
        plan(tab,n);
    }
    return 0;
}
