#ifndef DEK_H_INCLUDED
#define DEK_H_INCLUDED
#include <iostream>
#include <math.h>
using namespace std;
class tictactoe {
  public:
    tictactoe();
    enum pole {puste=' ', kolo='O', krzyz='X'};
    bool sprawdz(int x, int y, pole g);
    void ruch(int x, int y,pole g);
    void ruchpc();
    void plan();
    void start();
    void kopiuj(pole mama[3][3],pole dzidzia[3][3]);
    int ruchpc2(pole test[3][3],int x,int y);
    int ruchpc3(pole test[3][3],int x,int y);
    bool sprawdz2(pole g,pole test[3][3]);
  private:
    pole tab[3][3];
};

#endif // DEK_H_INCLUDED
