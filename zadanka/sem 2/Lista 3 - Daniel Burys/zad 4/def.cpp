#include "dek.h"
#include <iostream>
#include <windows.h>
using namespace std;

tictactoe::tictactoe()
    :tab( {
    {puste,puste,puste},
    {puste,puste,puste},
    {puste,puste,puste}
}) {
};


bool tictactoe::sprawdz(int x, int y, pole g) {
    if(  tab[x][0]==g&&tab[x][1]==g&&tab[x][2]==g||
            tab[0][y]==g&&tab[1][y]==g&&tab[2][y]==g||
            tab[2][2]==g&&tab[1][1]==g&&tab[0][0]==g||
            tab[0][2]==g&&tab[1][1]==g&&tab[2][0]==g)
        return 1;
    else
        return 0;
}
bool tictactoe::sprawdz2(pole g,pole test[3][3]) {
    if(     test[0][0]==g&&test[0][1]==g&&test[0][2]==g||//poziom1
            test[1][0]==g&&test[1][1]==g&&test[1][2]==g||//poziom2
            test[2][0]==g&&test[2][1]==g&&test[2][2]==g||//poziom3
            test[0][0]==g&&test[1][0]==g&&test[2][0]==g||//pion1
            test[0][1]==g&&test[1][1]==g&&test[2][1]==g||//pion2
            test[0][2]==g&&test[1][2]==g&&test[2][2]==g||//pion3
            test[2][2]==g&&test[1][1]==g&&test[0][0]==g||
            test[0][2]==g&&test[1][1]==g&&test[2][0]==g)
        return 1;
    else
        return 0;
}

void tictactoe::ruch(int x, int y,pole g) {
    tab[x][y]=g;
}

void tictactoe::kopiuj(pole mama[3][3],pole dzidzia[3][3]) {
    int i,j;
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            dzidzia[i][j]=mama[i][j];
        }
    }
}

void tictactoe::ruchpc() {
    int i,j;
    pole test[3][3];
    int wynik[3][3]= {0};
    kopiuj(tab,test);
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=krzyz;
                if(sprawdz2(krzyz,test)) {
                    tab[i][j]=krzyz;
                    return;
                }
                test[i][j]=puste;
            }
        }
    }

    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=kolo;
                if(sprawdz2(kolo,test)) { // ruch blokujacy wygrana przeciwnika
                    tab[i][j]=krzyz;
                    test[i][j]=puste;
                    return;
                }
                test[i][j]=puste;
            }
        }
    }

    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=krzyz;
                wynik[i][j]=ruchpc3(test,i,j);
                test[i][j]==puste;
            }
        }
    }
    int n=0,m=0;
        for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(wynik[i][j]<=wynik[n][m]&&tab[i][j]==puste) {// zeby nie wybralo pola w lewym gornym rogu
                n=i,m=j;
            }
        }}
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(wynik[i][j]>=wynik[n][m]&&tab[i][j]==puste) {// ktory ruch jest najlepszy
                n=i,m=j;
            }
        }
    }
    //cout<<endl<<test[0][0]<<" "<<test[0][1]<<" "<<test[0][2]<<" "<<test[1][0]<<" "<<test[1][1]<<" "<<test[1][2]<<" "<<test[2][0]<<" "<<test[2][1]<<" "<<test[2][2];cin>>i;
    tab[n][m]=krzyz;
//cout<<endl<<n<<m;cin>>i;
    return;
}

int tictactoe::ruchpc2(pole test[3][3],int x,int y) {// symulowany ruch komputera
    int i,j,k=0;
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=krzyz;
                if(sprawdz2(krzyz,test)) {
                    test[i][j]=puste;
                    test[x][y]=puste;
                    return 1;
                } else
                    test[i][j]=puste;
            }
        }
    }
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=kolo;
                if(sprawdz2(kolo,test)) { // ruch blokujacy wygrana przeciwnika
                    test[x][y]=puste;
                    return ruchpc3(test,i,j);
                }
                test[i][j]=puste;
            }
        }
    }
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=krzyz;
                k+=ruchpc3(test,i,j);// gdzies tu blad jest
                test[i][j]=puste;
            }
        }
    }
    test[x][y]=puste;
    return k;
}

int tictactoe::ruchpc3(pole test[3][3],int x,int y) { // symulowany ruch gracza
    int i,j,k=0;
    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=kolo;
                if(sprawdz2(kolo,test)) {
                    test[i][j]=puste;
                    test[x][y]=puste;
                    return -1;
                }
                test[i][j]=puste;
            }
        }
    }

    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {
                test[i][j]=krzyz;
                if(sprawdz2(krzyz,test)) { // ruch blokujacy wygrana przeciwnika
                    test[x][y]=puste;
                    return ruchpc2(test,i,j);
                }
                test[i][j]=puste;
            }
        }
    }

    for(i=0; i<3; i++) {
        for(j=0; j<3; j++) {
            if(test[i][j]==puste) {  // albo tu
                test[i][j]=kolo;
                k+=ruchpc2(test,i,j);
                test[i][j]=puste;
            }
        }
    }
    test[x][y]=puste;
    return k;
}

void tictactoe::plan() {
    cout<<(char)tab[0][0]<<" | "<<(char)tab[0][1]<<" | "<<(char)tab[0][2]<<endl;
    cout<<"----------"<<endl;
    cout<<(char)tab[1][0]<<" | "<<(char)tab[1][1]<<" | "<<(char)tab[1][2]<<endl;
    cout<<"----------"<<endl;
    cout<<(char)tab[2][0]<<" | "<<(char)tab[2][1]<<" | "<<(char)tab[2][2]<<endl;
}

void tictactoe::start() {
    tictactoe();
    int a,i;
    int b;
    cout<<"Kto zaczyna? (1-ty,2-komputer):";
    cin>>i;
    if(i==2)
        goto A;
    while(1==1) {
        system("cls");
        plan();
        while(1==1) {
            cout<<endl<<"gracz 1(O):"<<endl;
            cout<<"wiersz:";
            cin>>a;
            a--;
            cout<<"kolumna:";
            cin>>b;
            b--;
            if(a>2||b>2||a<0||b<0) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            if(tab[a][b]==krzyz||tab[a][b]==kolo) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                ruch(a,b,kolo);
                break;
            }
        }
        if (sprawdz(a,b,kolo)) {
            cout<<"Gracz 1 wygral";
            break;
        }
A:
        system("cls");
        plan();
        ruchpc();

        if (sprawdz2(krzyz,tab)) {
            plan();
            cout<<"Gracz 2 wygral";
            break;
        }
        if(tab[0][0]!=puste&&tab[0][1]!=puste&&tab[0][2]!=puste&&tab[1][0]!=puste&&tab[1][1]!=puste&&tab[1][2]!=puste&&tab[2][0]!=puste&&tab[2][1]!=puste&&tab[2][2]!=puste) { //remis?
            plan();
            cout<<endl<< "Remis!";
            break;
        }
    }
}
