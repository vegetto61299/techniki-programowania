#ifndef DEK_H_INCLUDED
#define DEK_H_INCLUDED
#include <iostream>
#include <math.h>
using namespace std;
class tictactoe{
    public:
    tictactoe();
    enum pole{puste=' ', kolo='O', krzyz='X'};
    bool sprawdz(int x, int y, pole g);
    void ruch(int x, int y,pole g);
    void plan();
    void start();
    private:
    pole tab[3][3];
};
#endif // DEK_H_INCLUDED
