#include "dek.h"
#include <iostream>
#include <windows.h>
using namespace std;

tictactoe::tictactoe()
    :tab( {{puste,puste,puste}, {puste,puste,puste}, {puste,puste,puste}}) {
};


bool tictactoe::sprawdz(int x, int y, pole g) {
    if(  tab[x][0]==g&&tab[x][1]==g&&tab[x][2]==g||
            tab[0][y]==g&&tab[1][y]==g&&tab[2][y]==g||
            tab[2][2]==g&&tab[1][1]==g&&tab[0][0]==g||
            tab[0][2]==g&&tab[1][1]==g&&tab[2][0]==g
      )
        return 1;
    else
        return 0;
}

void tictactoe::ruch(int x, int y,pole g) {
    tab[x][y]=g;
}

void tictactoe::plan() {
    cout<<(char)tab[0][0]<<" | "<<(char)tab[0][1]<<" | "<<(char)tab[0][2]<<endl;
    cout<<"----------"<<endl;
    cout<<(char)tab[1][0]<<" | "<<(char)tab[1][1]<<" | "<<(char)tab[1][2]<<endl;
    cout<<"----------"<<endl;
    cout<<(char)tab[2][0]<<" | "<<(char)tab[2][1]<<" | "<<(char)tab[2][2]<<endl;
}

void tictactoe::start() {
    tictactoe();
    int a;
    int b;
    while(1==1) {
        system("cls");
        plan();
        while(1==1) {
            cout<<endl<<"gracz 1(O):"<<endl;
            cout<<"wiersz:";
            cin>>a;a--;
            cout<<"kolumna:";
            cin>>b;b--;
            if(a>2||b>2||a<0||b<0) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            }
            if(tab[a][b]==krzyz||tab[a][b]==kolo) {
                cout<<"Pole zajete"<<endl;
                continue;
            } else {
                ruch(a,b,kolo);
                break;
            }
        }
        if (sprawdz(a,b,kolo)) {
            cout<<"Gracz 1 wygral";
            break;
        }
        system("cls");
        plan();
        while(1==1) {
            cout<<endl<<"gracz 2(X):"<<endl;
            cout<<"wiersz:";
            cin>>a;a--;
            cout<<"kolumna:";
            cin>>b;b--;
            if(a>2||b>2||a<0||b<0) {
                cout<<"Pole niewlasciwe"<<endl;
                continue;
            } else if(tab[a][b]==krzyz||tab[a][b]==kolo) {
                cout<<"Pole zajete"<<endl;
                continue;
            }  else {
                ruch(a,b,krzyz);
                break;
            }
        }
        if (sprawdz(a,b,krzyz)) {
            cout<<"Gracz 2 wygral";
            break;
        }
    }
}
