#include"koronapp.hpp"
#include<iostream>
#include<cmath>
#define M_PI 4*atan(1.)

Point2d::Point2d()
    : _phi(0)
    , _r(0){
}

Point2d::Point2d(double x, double y){
    _phi=atan2(y,x);
    _r=sqrt(x*x+y*y);
}

Point2d::Point2d(const Point2d& other)
    : _r(other._r)
    , _phi(other._phi){
}

Point2d& Point2d::operator= (const Point2d& other){
    this->_r = other._r;
    this->_phi = other._phi;
    return *this;
}

double Point2d::getX(){
    return _r*cos(_phi);
}

double Point2d::getY(){
    return  _r*sin(_phi);
}

double Point2d::getR(){
    return _r;
}

double Point2d::getPhi(){
    return _phi;
}

void Point2d::setXY(double x, double y){
    _phi=atan2(y,x);
    _r=sqrt(x*x+y*y);
}

void Point2d::setRPhi(double r, double phi){
    _r = r;
    _phi = phi;
}
// przekrztalcenia
void Point2d::jednokladnosc(double k){
    _r = _r*k;
}
void Point2d::obrot(double k){
    _phi = _phi+k*M_PI;
}

std::ostream& operator<<(std::ostream& out, Point2d& p){
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
