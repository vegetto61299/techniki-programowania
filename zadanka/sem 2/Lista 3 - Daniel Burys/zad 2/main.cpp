#include <iostream>
#include "koronapp.hpp"
using namespace std;
int main()
{
    Point2d a;
    cout << "a = " << a << endl;
    a.setXY(3, 4);
    cout << "a = " << a << endl;
    Point2d b(-2,-1);
    cout << "b = " << b << endl;
    Point2d c(b);
    cout << "c = " << c << endl;
    cout << "a.x = "   << a.getX() << endl;
    cout << "a.y = "   << a.getY() << endl;
    cout << "a.r = "   << a.getR() << endl;
    cout << "a.phi = " << a.getPhi() << endl;
    b.setRPhi(5,1);
    cout << "b = " << b << endl;

    double k;
    cout<< "podaj k aby przekrztalcic za pomoca jednokladnosci punkt a:";
    cin>>k;
    a.jednokladnosc(k);
    cout<< "podaj phi aby obrocic punkt a o phi*pi:";
    cin>>k;
    a.obrot(k);
    cout << "a.x = "   << a.getX() << endl;
    cout << "a.y = "   << a.getY() << endl;
    cout << "a.r = "   << a.getR() << endl;
    cout << "a.phi = " << a.getPhi() << endl;

    return 0;
}
