#ifndef KORONAPP_HPP_INCLUDED
#define KORONAPP_HPP_INCLUDED
#include <iostream>

class Point2d{
    public:
        Point2d();

        Point2d(double, double);

        Point2d(const Point2d& other);

        Point2d& operator= (const Point2d& other);

        double getX();
        double getY();
        double getR();
        double getPhi();
        void setXY(double, double);
        void setRPhi(double, double);
        void jednokladnosc(double k);
        void obrot(double k);

    private:
        double _phi;
        double _r;
};


std::ostream& operator<<(std::ostream& out, Point2d& p);





#endif // KORONAPP_HPP_INCLUDED
