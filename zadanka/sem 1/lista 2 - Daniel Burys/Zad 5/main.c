#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main() {
    int p;
    printf("Podaj liczbe naturalna do rozlozenia na czynniki pierwsze: ");
    scanf("%d",&p);
    while (p<1) {
        printf("podaj liczbe naturalna: ");
        scanf("%d",&p);
    }

    int i=2;
    for(i; i<=p; i++) {
        if(i==sqrt(p)+1) {
            i=p;
        }
        if(p%i==0) {
            printf("%-7d%15s|%d\n",p,"",i);
            p=p/i;
            i--;
        }
    }
    printf("%-7d%15s|\n",1,"");
    return 0;
}
