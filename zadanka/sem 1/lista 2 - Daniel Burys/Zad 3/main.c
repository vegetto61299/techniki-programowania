#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int min(int *tab,int *b);
int max(int *tab,int *b);
float srednia(int *tab,int *b);
float mediana(int *tab,int *b);
float odchylenie(float sr, int *tab,int *b);
void podaj_ciag(int **tab,int *b);
void zrob_geo(int a, int *b, int r, int *tab);
void zrob_ary(int a, int *b, int r, int *tab);
void zrob_reczny(int *b, int *tab);
//******************majn***********
int main() {
    int *tab, b;
    podaj_ciag(&tab,&b);
    printf("\n\n**************************************\n\n");
    printf("min twojego ciagu: %d\n",min(tab,&b));
    printf("max twojego ciagu: %d\n",max(tab,&b));
    float sr=srednia(tab,&b);
    printf("srednia twojego ciagu: %.1f\n",sr);
    printf("odchylenie standardowe twojego ciagu: %.2f\n",odchylenie(sr,tab,&b));
    printf("mediana twojego ciagu: %.1f\n",mediana(tab,&b));

    return 0;
}
//******************def funkcji**********
int min(int *tab,int *b) {
    int min=tab[0];
    int i;
    for(i=1; i<*b; i++) {
        if(tab[i]<min) {

            min=tab[i];
        }
    }
    return min;
}
//************************************
int max(int *tab,int *b) {
    int max=tab[0];
    int i;
    for(i=1; i<*b; i++) {
        if(tab[i]>max) {
            max=tab[i];
        }
    }
    return max;
}
//************************************
float srednia(int *tab,int *b) {
    int i;
    float srednia=tab[0];
    for(i=1; i<*b; i++) {
        srednia+=(float) tab[i];
    }
    srednia=srednia/(float) *b;
    return srednia;
}
//****
float mediana(int *tab,int *b) {
    int i=0;
    int j=*b;
    float mediana;
    int kingbruceleekaratemistrz;
    for(j; j>=1; j--){//sortowanie, *b zmienione na j by nie zmienia� b
        for(i; i<j-1;i++) {
            if(tab[i]>tab[i+1]) {
                kingbruceleekaratemistrz=tab[i];
                tab[i]=tab[i+1];
                tab[i+1]=kingbruceleekaratemistrz;
            }
        }
        i=0;
    }//koniec sortowania

    for(i=0; i<(*b); i++) {
        printf("%d, ",tab[i]);
    }

    if (*b%2==0) {
        mediana=(float) (tab[*b/2]+tab[*b/2-1])/2;
    } else {
        mediana=tab[*b/2];
    }
    return mediana;
}
//*********************************
float odchylenie(float sr, int *tab,int *b) {
    float odchylenie=0;
    int i;
    for(i=0; i<*b; i++) {
        odchylenie+=pow((tab[i]-sr),2);
    }
    odchylenie=odchylenie/(*b);
    odchylenie=sqrt(odchylenie);
    return odchylenie;
}
//************************************
void podaj_ciag(int **tab,int *b) {
    int ciag,a,r;
    printf("Jakiego typu ciag liczb calkowitych chcesz sprawdzic\n    1.Ciag arytmetyczny\n    2.Ciag geometryczny\n    3.Ciag wpisany recznie\n");

    while(ciag!=1&&ciag!=2&&ciag!=3) {
        printf("Podaj typ ciagu: ");
        scanf("%d",&ciag);
    }

    if(ciag==1||ciag==2) {
        printf("Podaj pierwszy wyraz ciagu: ");
        scanf("%d",&a);

        printf("Podaj roznice/iloraz ciagu liczb calkowitych: ");
        scanf("%d",&r);
    }
    printf("Podaj ilosc liczb w ciagu: ");
    scanf("%d",&*b);

    *tab=malloc(*b*sizeof(int));
    if(ciag==2) {
        zrob_geo(a,*&b,r,*tab);
    } else if(ciag==1) {
        zrob_ary(a,*&b,r,*tab);
    } else if(ciag==3) {
        zrob_reczny(*&b,*tab);
    }
    printf("\n\n**************************************\n\n");
    printf("Twoj ciag to: \n");
    int i=0;
    for(i; i<(*b); i++) {
        printf("%d, ",(*tab)[i]);
    }
}
//************************************
void zrob_ary(int a,int *b,int r, int *tab) {
    int i=0;
    for (i; i<*b; a+=r) {
        tab[i]=a;
        i++;
    }
}
//************************************
void zrob_geo(int a,int *b,int r, int *tab) {
    int i=0;
    for(i; i<*b; a*=r) {
        tab[i]=a;
        i++;
    }
}
//************************************
void zrob_reczny(int *b, int *tab) {
    int i=0;
    for (i; i<*b; i++) {
        printf("Podaj %d liczbe ciagu: ",i+1);
        scanf("%d",&tab[i]);
    }
}
