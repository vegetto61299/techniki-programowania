#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    int p;
    printf("Podaj liczbe naturalna: ");
    scanf("%d",&p);
    while (p<0) {
        printf("Podaj liczbe naturalna: ");
        scanf("%d",&p);
    }
    printf("Podaj liczby pierwsze mniejsze od podanej liczby:\n");
    int i;
    int j;
    int franky;
    for(i=1; i<p; i++) {

        for(j=1; j<=i; j++) {
            if(i%j==0) {
                franky++;
            }
        }
        if (franky==2) {
            printf("%d\n",i);
        }
        franky=0;
    }


    return 0;
}
