#include <stdio.h>
#include <stdlib.h>
float licz(int n,float x,float tab[]);
//***************************
int main() {
    float x;
    int n;
    printf("podaj stopien wielomianu: ");
    scanf("%d",&n);
        while(n<0) {
            printf("podaj liczbe naturalna jako stopien wielomianu: ");
            scanf("%d",&n);
        }
    float *tab;
    tab=malloc((n+1)*sizeof(*tab));

    printf("podaj wspolczynnik x: ");
    scanf("%f",&x);

    int i=0;
    for(i; i<=n; i++) {
        printf("podaj %d wspolczynnik wielomianu: ",i+1);
        scanf("%f",&tab[i]);
    }
    printf("\nWynikiem wielomianu jest: %g",licz(n,x,tab));
    return 0;
}
//***************************
float licz(int n,float x,float tab[]) {
    float wynik=0;
    int i=0;
    if(x==0) {
        wynik=tab[n];
    }
    if(x!=0) {
        for(i; i<=n; i++) {
            wynik=wynik*x+tab[i];
        }
    }

    return wynik;
}
