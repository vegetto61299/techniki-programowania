#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double nawias(double x);
double beznawias(double x);
void wypisz();
//**********************
int main()
{
    wypisz();
    return 0;
}
//*************************
void wypisz(){
    double i;
    printf("%s%15s%23s%15s","krok","(x-1)^7","rozbity wzor","roznica\n");
    for(i=0.99;i<=1.01;i+=0.0001){
        printf("x=%.4f",i);
        printf("%18e",nawias(i));
        printf("%18e",beznawias(i));
        printf("%18e\n",fabs(nawias(i)-beznawias(i)));
    }
    printf("%s%16s%23s%14s","krok","(x-1)^7","rozbity wzor","roznica\n");
}


//*************************
double nawias(double x){
    double wynik=pow(x-1,7);
    return wynik;
}


//*************************
double beznawias(double x){
    double wynik;
    wynik=pow(x,7)-7*pow(x,6)+21*pow(x,5)-35*pow(x,4)+35*pow(x,3)-21*pow(x,2)+7*pow(x,1)-1;
    return wynik;
}
