#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    int p;
    int a=0;
    int **tab;
    tab=calloc(11,sizeof(int*));
    int i;
    for(i=0; i<=10; i++) {
        tab[i]=calloc(2,sizeof(int));
    }
    tab[0][0]++;  // dla 1
    tab[0][1]++;


    printf("Podaj liczbe naturalna do rozlozenia na czynniki pierwsze: ");
    scanf("%d",&p);
    while (p<1) {
        printf("podaj liczbe naturalna: ");
        scanf("%d",&p);
    }

    for(i=2; i<=(p); i++) {
        if(i==sqrt(p)+1) {
            i=p;
        }
        if(p%i==0) {
            if(i==tab[a][0]) {
                tab[a][1]++;
            } else if(i!=tab[a][0]) {
                a++;
                tab[a][0]=i;
                tab[a][1]++;
            }

            printf("%-7d%10s|%d\n",p,"",i);
            p=p/i;
            i--;
        }
    }
    printf("%-7d%10s|\n",p,"");

    printf("Czynniki i ich krotnosc: ");
    for(i=0; i<=a; i++) {
        printf("%d^(%d), ",tab[i][0],tab[i][1]);
    }

    return 0;
}
