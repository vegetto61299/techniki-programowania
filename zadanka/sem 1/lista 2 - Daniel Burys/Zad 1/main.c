#include <stdio.h>
#include <stdlib.h>


float fmin();
double dmin();

//***********************main*************************

int main()
{
    printf("Najmniejszy float: %e\n",fmin());
    printf("Najmniejszy double: %e",dmin());
    return 0;
}



//************************Funkcje*******************

float fmin(){
float fm,f;
    f=1;
    while(f!=0){
        fm=f;
        f=f/2;
    }
    return fm;
}


double dmin(){
double dm,d;
    d=1;
    while(d!=0){
        dm=d;
        d=d/2;
    }
    return dm;
}
