#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "dek.h"


int main() {
    int n;
    printf("Podaj ilosc liczb: \n");
    scanf("%i",&n);
    int tab[n];


    for(int i=0; i<n; i++) {
        printf("\nPodaj %d liczbe: ",i+1);
        scanf("%i",&tab[i]);
    }
    //Sortowanie
    if(n==1) {
        printf("Ta tablica ma 1 znak");
    }
    if(n==2) {
        sort2(tab,0);
    }
    if(n==3) {
        sort3(tab,0);
    }
    if(n==4) {
        sort4(tab,0);
    }
    if(n==5) {
        sort5(tab,0);
    }
    if(n>=6){
        int pom[n]; // tablica pomocnicza aby nie popsuc taba przy sortowaniu
        sort(pom,tab,0,n-1);
    }
            printf("Posortowane:");
        for(int i=0; i<n; i++) {
            printf("%d, ",tab[i]);
        }
    return 0;
}
