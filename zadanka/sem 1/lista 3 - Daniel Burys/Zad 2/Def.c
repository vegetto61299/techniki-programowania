#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "dek.h"

void add(stos *s,char x) {
    stos *ptr=malloc(sizeof(stos));
    ptr->x=x;
    ptr->next=NULL;
    if(s->next==NULL) {
        s->next=ptr;
    } else {
        ptr->next=s->next;
        s->next=ptr;
    }
    printf("\nDodano %c",x);
}
char read(stos *s) {
    if (s->next!=NULL) {
        return s->next->x;
    } else {
        return "";
    }

}
void del(stos *s) {
    if(s->next!=NULL) {
        printf(" - Usunieto %c ",s->next->x);
        stos *ptr=s->next;
        s->next=ptr->next;
        free(ptr);
    }
}
void burn(stos *s) {
    while(s->next!=NULL) {
        stos *ptr=s->next;
        s->next=ptr->next;
        free(ptr);
    }
}
void poka(stos *s) {
    stos *ptr;
    ptr=s->next;
    int h=0;
    while(ptr!=NULL) {
        printf("%c",ptr->x);
        ptr=ptr->next;
        h++;
    }
    free(ptr);
}



