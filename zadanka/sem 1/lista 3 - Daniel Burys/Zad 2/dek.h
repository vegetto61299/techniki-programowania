#ifndef DEK_H_INCLUDED
#define DEK_H_INCLUDED


typedef struct stos
{
  char x;
  struct stos *next;
} stos;
//************************************
void add(stos *s, char x);
char read(stos *s);
void del(stos *s);
void burn(stos *s);
void poka(stos *s);
#endif // DEK_H_INCLUDED
