
#include <stdio.h>
#include <stdlib.h>
#define C_const 299792458.
#define C_name 'c'
#define C_unit "m/s"
#define C_desc "Predkosc swiatla"

#define G_const 6.6742868e-11
#define G_name 'G'
#define G_unit "m^3/kg/s^2"
#define G_desc "Stala grawitacji"

#define R_const 8.31447215
#define R_name 'R'
#define R_unit "J*/mol/K"
#define R_desc "Stala gazowa"

#define F_const 96485.339915
#define F_name 'F'
#define F_unit "C/mol"
#define F_desc "Stala Faradaya"

#define K_const 1.380649e-23
#define K_name 'k'
#define K_unit "J/K"
#define K_desc "Stala Boltzmana"

#define E_const 1.602176634e-19
#define E_name 'e'
#define E_unit "C"
#define E_desc "Ladunek elementarny"

int main()
{
    char format[]="%-16c %-23g %-23s %s  \n";
    printf("Tabela stalych fizycznych\n");
    printf("Nazwa \t\t Warosc \t\t Jednostka \t\t Opis\n");
    printf(format,C_name,C_const,C_unit,C_desc);
    printf(format,G_name,G_const,G_unit,G_desc);
    printf(format,R_name,R_const,R_unit,R_desc);
    printf(format,F_name,F_const,F_unit,F_desc);
    printf(format,K_name,K_const,K_unit,K_desc);
    printf(format,E_name,E_const,E_unit,E_desc);

    return 0;
}
